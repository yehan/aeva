//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
/**
 * @class   mbObjectFactory
 * @brief   is a factory for implementing derived instances of vtk classes
 *
 * VTK classes can be designed to use the vtkObjectFactory to construct new
 * instances by using the vtkObjectFactoryNewMacro to define the class's New()
 * static method. This pattern allows for upstream libraries (like those coming
 * from ParaView) to implement instances of derived classes (like those coming
 * from ModelBuilder). mbObjectFactory is a means to inform vtkObjectFactory of
 * derived classes that should be used in favor of their base class
 * implementations.
*/
#ifndef aeva_mbObjectFactory_h
#define aeva_mbObjectFactory_h

#include "vtkObjectFactory.h"

class mbObjectFactory : public vtkObjectFactory
{
public:
  static mbObjectFactory* New();
  vtkTypeMacro(mbObjectFactory, vtkObjectFactory)

    const char* GetDescription() override
  {
    return "aeva factory overrides.";
  }

  const char* GetVTKSourceVersion() override;

  void PrintSelf(ostream& os, vtkIndent indent) override;

protected:
  mbObjectFactory();

private:
  mbObjectFactory(const mbObjectFactory&) = delete;
  void operator=(const mbObjectFactory&) = delete;
};

#endif
