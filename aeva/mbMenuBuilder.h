//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef aeva_mbMenuBuilder_h
#define aeva_mbMenuBuilder_h

class QMenu;
class QWidget;
class QMainWindow;

/**\brief Build menu items for AEVA.
  *
  * This is used to supplement ParaView's menu builder for those
  * menus which differ from the base ParaView application.
  */
class mbMenuBuilder
{
public:
  /**\brief Build the File menu.
    *
    */
  static void buildFileMenu(QMenu& menu);

  /**\brief Build the Help menu.
    *
    * We replace the ParaView-supplied builder since it contains
    * entries we do not want.
    * This method is also responsible for the "About aeva..." menu
    * item, which on macos platforms is not placed in the help menu.
    */
  static void buildHelpMenu(QMenu& menu);
};

#endif
