#CMB 6.1 Release Notes
CMB 6.1 is a minor release with new features.  Note any non-backward compatible changes are in bold italics. See also [CMB 6.0 Release Notes](cmb-6.0.md).


## CMB 6.1 Dependencies
* CMB 6.1 is based on SMTK 3.1 - For changes made to SMTK 3.1 please see the release notes in SMTK
* CMB 6.1 is based on ParaView 5.6

## Modeling Simulation Attributes
###Adding Exclusions and Prerequisites
Attribute Definitions can now provide mechanisms for modeling exclusion and prerequisites constraints.  An exclusion constraint prevents an attribute from being associated to same persistent object if another attribute is already associated and its definition excludes the attribute's type.  For example, consider two attribute definitions A and B as well as two attributes a (of type A) and b (or type B).  If we have specified that A and B excludes each other then if we have associated a to an object, we are prevented from associating b to the same object.

In the case of a prerequisite, an attribute is prevent from being associated if there is not an attribute of an appropriate type associated with the object.  So continuing the above example lets assume we add a new definition C that has A as it prerequisite and an attribute c (type C).  c can be associated to an object only if a is already associated to it.

In addition, these properties are also inherited.  So if we derive type A1 from A in the above example, an attribute a1 (from type A1), would excludes b and would be considered a prerequisite of c.

**NOTE - the exclusion property should be symmetric (if A doesn't allow B then B shouldn't allow A) but the prerequisite property can not be symmetric (else neither could be associated - if you need to have both attributes always assigned together then the information should be modeled as a single definition)**
### Analyses can have parents
Added the concept of Parent Analysis. The parent relationship is used to determine the categories associated with an analysis.  A child will inherit all of its parent's categories.

## Changes to Model Resource System
### Assign Colors Operation now supports opacity

The "assign colors" operation now provides a way to set opacity independently
  of or in tandem with colors. The user interface takes advantage of this to
  provide an opacity slider.

### Added new query options
Associations and Reference-based Item constraints can now include property values associated with model resource components.

##Changes to Views and QT Extensions
###New View Type - Associations
This view has the same syntax as an Attribute View but only allows the user to change the association information of the attribute resulting in taking up less screen Real Estate

###New View Type - Analysis
An Analysis View is a specialized view for choosing the types of analyses the user wants to perform.  These choices are persistent and can be used by an export operation instead of having the operator ask the user what types of analyses should be performed.

Unlike other views the Analysis View will construct both an Attribute Definition and corresponding Attribute when needed.  The Attribute Definition is based on the Analysis Information stored in the Attribute Resource.  Any Analysis that is referred to by another will be represented as a Group Item.  All other Analyses will be represented as a Void Item.

The View also controls which categories are permitted to be displayed and/or selected.  The set is union of all of the selected Analyses' categories.

The following is an example of a Analysis View:

```xml
 <View Type="Analysis" Title="Analysis" AnalysisAttributeName="truchasAnalysis"
 AnalysisAttributeType="truchasAnalysisDefinition">
</View>
```

  * AnalysisAttributeType is the name of the Attribute Definition the view will create to represent the Analysis Structure (if needed)
  * AnalysisAttributeName is the name of the Attribute the view will create to represent the Analysis  (if needed)


###Changes to Attribute View
* added a new XML attribute "HideAssociations".  If set to true the view will not display the association editing widget save screen Real Estate
* If there is only one type of attribute being created/modified then the type column is no longer displayed
* For the time being the view by property  feature has been disabled until we can decide on whether it is useful and if so, what is the best way to display the information.
* The column "Attribute" has been renamed to "Name"
* Attempting to rename an attribute to a name already is use now generates a warning dialog.

###Changes to Group View
* View no longer displays empty tabs
* Current tabs are now remembered when the group rebuilds its widget - previously this was only true for the top-level tabbed group views

##Item Changes
####ReadOnly View Items
Added a new ReadOnly Option to Item Views.  In the following example the item, absolute-zero, in the attribute physical-constants has been made read only.  The current implementation disables the widgets defined by the read only item from being  modified but will still display them.

```xml
    <View Type="Instanced" Title="Global Constants">
      <InstancedAttributes>
        <Att Name="physics" Type="physics">
          <ItemViews>
            <View Item="fluid-body-force" Type="Default" FixedWidth="0"/>
          </ItemViews>
        </Att>
        <Att Name="physical-constants" Type="physical-constants">
          <ItemViews>
            <View Item="absolute-zero" Type="Default" ReadOnly="true" FixedWidth="50/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
```
####Changes to Displaying Items
* Extensible Group Items now display "Add Row" instead of "Add sub group" - this label can be changed using an ItemView with the XML attribute : ExtensibleLabel
* Added a FixedWidth Option for String, Double and Int ItemViews as also shown in the above example - **Note: setting the fixed width to 0 means there is no fixed width.**
* qtReferenceItem now allows developers to override the visibility icons
  with custom URLs. See qtReferenceItem::setSelectionIconPaths() and
  doc/userguide/attribute/file-syntax.rst for details.

####Highlighting when Hovering
* Views showing associations will now highlight geometry when the user hovers over it

#### New ViewItem Styles
##### Box Widget

The box widget now accepts a single DoubleItem (with 6 entries)
specifying an axis-aligned bounding box or a GroupItem
containing multiple DoubleItems that configure a bounding box
in different ways depending on how they are used.
See the pqSMTKBoxItemWidget header for details.

##### Infinite Cylinder Widget
There is now an infinite cylinder widget, which can be bound to a Group
item containing 3 Double children that serve as a center point,
a direction vector, and a radius.
Note that the widget models a cylinder of infinite length cut
by a bounding box whose size is not currently specified.

##### Cone-Frustum Widget
There is now a cone-frustum widget which can be bound to a Group
item containing 3 or 4 Double children that server as endpoints
and endpoint radii. The cone has a special case for cylinders where
only 1 radius value is provided.


#### Using QPointer it qtItem and AttributeItemInfo
* ***AttributeItemInfo::parentWidget() now returns a QPointer\<QWidget>***
* ***qtItem::parentWidget() now returns a QPointer\<QWidget>***
* ***qtItem::widget() now returns a QPointer\<QWidget>***

These changes were done to improve general stability as well as to simplifying when a qtItem needs to delete its main widget.


## Known Issues
### Closing Resources
If you close a resource whose components are being referenced by an attribute resource and then reload the resource into ModelBuilder, the associated information may look incorrect.  For example a temperature attribute that is supposed to be associated with Face1 of a model resource will not show the association and in fact will not list Face1 as being available to be associated if the model resource is closed and then re-opened.  The underlying information stored in the attribute resource is correct; however, the reference items (including those used to hold associations) are keeping the original model resource in memory.  A tested workaround for this issue is to simply close and reload the attribute resource as well.
