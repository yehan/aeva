================
aeva User's Guide
================

The Annotation and Exchange for Virtual Anatomy application -- aeva for short --
exists to accept patient data and annotations on that data in a wide variety of
formats.

Currently, aeva can read

+ Nifti files holding volumetric image data
+ MED files holding unstructured surface and volume meshes
+ Exodus files holding finite element data

and provides a simple schema for annotating knee joints
via the included SMTK attribute.

.. toctree::
   :maxdepth: 4
