#!/bin/zsh

src=`pwd`/../aeva/resource/aeva-logo.svg
dst=`pwd`/../aeva/aeva.iconset
mkdir $dst
for res in 16 32 128 256 512; do
  xx=$((2 * res))
  inkscape --export-png=${dst}/icon_${res}x${res}.png -w ${res} -h ${res} -y 0.0  ${src}
  inkscape --export-png=${dst}/icon_${res}x${res}@2x.png -w ${xx} -h ${xx} -y 0.0 ${src}
done
iconutil --convert icns `pwd`/../aeva/aeva.iconset
