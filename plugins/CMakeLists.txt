paraview_plugin_scan(
  PLUGIN_FILES postprocessing-mode/paraview.plugin
  PROVIDES_PLUGINS paraview_plugins
  ENABLE_BY_DEFAULT ON
  HIDE_PLUGINS_FROM_CACHE ON)
paraview_plugin_build(
  PLUGINS ${paraview_plugins}
  PLUGINS_FILE_NAME "aeva.xml"
  INSTALL_EXPORT aevaParaViewPlugins
  RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR}
  CMAKE_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/aeva
  LIBRARY_SUBDIRECTORY "aeva-${aeva_version}"
  TARGET aeva_paraview_plugins)
